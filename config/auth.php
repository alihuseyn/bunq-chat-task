<?php

return [
    'defaults' => [
        'guard' => 'api',
        'passwords' => 'users',
    ],

    'guards' => [
        'api' => [
            'driver' => 'bunq',
            'provider' => 'users',
        ],
    ],

    'providers' => [
        'users' => [
            'driver' => 'bunq'
        ]
    ]
];
