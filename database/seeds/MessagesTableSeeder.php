<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Message;

class MessagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds and generate 5 messagings
     * between all users which available on database
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();
        for ($i = 0; $i < count($users); $i += 1) {
            $sender = $users[$i];
            for ($j = 0; $j < count($users); $j += 1) {
                if ($users[$j]->id == $sender->id) {
                    continue;
                }
                $receiver = $users[$j];
                factory(Message::class, 5)->create([
                    'conversation_id' => $sender->id + $receiver->id,
                    'sender_id' => $sender->id,
                    'receiver_id' => $receiver->id
                ]);
            }
        }
    }
}
