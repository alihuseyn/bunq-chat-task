<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('conversation_id');
            $table->unsignedInteger('sender_id');
            $table->foreign('sender_id')
                ->references('id')
                ->on('users');
            $table->unsignedInteger('receiver_id');
            $table->foreign('receiver_id')
                ->references('id')
                ->on('users');
            $table->text('message');
            $table->timestamp('deleted_for_sender')->nullable();
            $table->timestamp('deleted_for_receiver')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
