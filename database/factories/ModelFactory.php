<?php

use App\Models\User;
use App\Models\Message;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Hash;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'username' => $faker->username,
        'password' => '123456'
    ];
});

$factory->define(Message::class, function (Faker $faker) {
    return ['message' => $faker->text(100)];
});
