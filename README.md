# Bunq Chat App

The application allows users to log in and chat with each other over Restful API.
Lumen microframework is selected for development. Its size and performance
and predefined functionalities make me select this framework.

## Requirements

The list below stated are requirements for the project
 - php
 - sqlite
 - composer

## Configure Application with Docker Image
### Requirement
 - docker


1- Generate new `.env` file with content as below
```
APP_NAME=Lumen
APP_ENV=production
APP_KEY=base64:MNefnaPvV6ur6VXsvjReME39JTjEFrBH1yWxAhP/QiM=
APP_DEBUG=false
APP_URL=http://localhost
APP_TIMEZONE=UTC

LOG_CHANNEL=stack
LOG_SLACK_WEBHOOK_URL=

DB_CONNECTION=sqlite
DB_PREFIX=bunq_

JWT_TTL=1
JWT_SECRET=7d93f5cd5747c5f39effd1b4cc2d9113bed31e9575500c02a3b1fa0de16e4474
JWT_JTI=d4a03a-3fe399-d29787-013aa6-219f0f
JWT_ISS=http://localhost
JWT_EXP_CHECK=10

CACHE_DRIVER=file
QUEUE_CONNECTION=sync
```

2- Generate image from `Dockerfile`
```sh
docker build . -t bunq/chat-app-task
```

3- Create application container from image
```sh
docker run -d -p 8000:80 --name bunq-chat-app-task bunq/chat-app-task
```

## Configure Application In Local

1- Install project package requirements
```sh
$ composer install
```

2- Generate new `.env` file with content as below
```
APP_NAME=Lumen
APP_ENV=production
APP_KEY=base64:MNefnaPvV6ur6VXsvjReME39JTjEFrBH1yWxAhP/QiM=
APP_DEBUG=false
APP_URL=http://localhost
APP_TIMEZONE=UTC

LOG_CHANNEL=stack
LOG_SLACK_WEBHOOK_URL=

DB_CONNECTION=sqlite
DB_PREFIX=bunq_

JWT_TTL=1
JWT_SECRET=7d93f5cd5747c5f39effd1b4cc2d9113bed31e9575500c02a3b1fa0de16e4474
JWT_JTI=d4a03a-3fe399-d29787-013aa6-219f0f
JWT_ISS=http://localhost
JWT_EXP_CHECK=10

CACHE_DRIVER=file
QUEUE_CONNECTION=sync
```

3- Generate database tables. 

Note: It is already filled with tables and data you can clean with `php artisan migrate:rollback` before continuing with any database-related operation. If you want to continue with available database then skip 4th and 5th explanation

```
$ php artisan migrate
```

4- (**Optional**) Fill the database with seed data

```
$ php artisan db:seed
```

5- Run the application

```
$ composer run server
```

## API Documentation

After configuration and successful run in browser type [http://localhost:8000/docs](http://localhost:8000/docs).
The swagger documentation for API will be seen inside of the tab and will allow you to make a request and observe a response.

### Cache Usage
In some parts of endpoints, the cache is used to improve performance. It is a file-based cache mechanism.
It is mostly used for current user fetch and conversations. As we know inside of the application
we mostly refer to the current user and each time fetching this information from the database is not better which makes database overloaded.

### Authentication
JWT based authentication is used to support stateless of Restful API. The usage is simple and just allows the token to be
valid an hour and will be expired after that. But during a request, if the token has seen be invalid after 10 minutes the new token gets generated and responded with `X-NEW-TOKEN` header to persist connection of user with a chat application. After logout, the token is put to blacklist until expire

### Conversation
If a user wants to write another user should start a conversation. After the conversation request calculated identifier is generated.
This id is a simple  and just summation of receiver and sender ids'. User can delete conversation but this deletion will not
affect other users only will be soft-deleted for current user.

### Message
Allow users to write a message to another user if the conversation already defined. User can delete a message for itself without affecting
others. 

### Contact
Allow retrieving available users in chat.

## Database Structure

Simple 2 tables are used to generate a relational database structure.

```
|------------|                        |----------------------|
| bunq_users |                        |     bunq_messages    |
|------------| 1                 1..* |----------------------|
| PK: id     | o--------------------> | PK: id               |
| username   | 1                 1..* | conversation_id      |
| password   | o--------------------> | FK: sender_id        |
| created_at |                        | FK: receiver_id      |
| update_at  |                        | message              |
|------------|                        | deleted_for_sender   |
                                      | deleted_for_receiver |
                                      |----------------------|
```

One-to-Many between User and Messages tables.
