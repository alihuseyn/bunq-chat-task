<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('docs', function () {
    return view('docs');
});

$router->group(['prefix' => 'auth'], function () use ($router) {
    $router->post('login', 'LoginController@login');
    $router->post('register', 'RegisterController@register');
});

$router->group(['middleware' => ['auth', 'jwt-expire']], function () use ($router) {
    $router->group(['prefix' => 'auth'], function () use ($router) {
        $router->post('logout', 'LoginController@logout');
        $router->get('me', 'ProfileController@me');
    });

    $router->group(['prefix' => 'conversations'], function () use ($router) {
        $router->post('/', 'ConversationController@create');
        $router->get('/', 'ConversationController@index');

        $router->group(['prefix' => '{cid:[0-9]+}'], function () use ($router) {
            $router->get('/', 'ConversationController@show');
            $router->delete('/', 'ConversationController@destroy');
            
            $router->post('messages', 'MessageController@create');
            $router->get('messages/{mid:[0-9]+}', 'MessageController@show');
            $router->delete('messages/{mid:[0-9]+}', 'MessageController@destroy');
        });
    });

    $router->get('contacts', 'ContactController@index');
});
