<?php

namespace App\Exceptions;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param \Throwable $exception Exception
     *
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param \Illuminate\Http\Request $request   Request
     * @param \Throwable               $exception Exception
     *
     * @return \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        if (!env('APP_DEBUG')) {
            $code = $exception->getCode() == 0 ? 400 : $exception->getCode();
            $errors = ['message' => $exception->getMessage()];

            if ($exception instanceof NotFoundHttpException) {
                $code = 404;
                $errors['message'] = 'Not found';
            }

            if ($exception instanceof MethodNotAllowedHttpException) {
                $code = 405;
                $errors['message'] = 'Method not allowed';
            }

            if ($exception instanceof ValidationException) {
                $errors = [];
                foreach ($exception->errors() as $key => $error) {
                    $errors = array_merge($errors, [$key => head($error)]);
                }
            }

            return response(['errors' => $errors ], $code);
        }

        return parent::render($request, $exception);
    }
}
