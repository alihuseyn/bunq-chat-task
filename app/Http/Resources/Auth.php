<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @OA\Schema(
 *     schema="AuthResponse",
 *     type="object",
 *     title="Auth",
 *     properties={
 *         @OA\Property(property="token", type="string"),
 *         @OA\Property(property="expire_in", type="integer"),
 *         @OA\Property(property="token_type", type="string")
 *     }
 * )
 */
class Auth extends JsonResource
{
    /**
     * No wrapper will be used
     *
     * @var string
     */
    public static $wrap = null;

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request Request instance
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'token' => $this->resource,
            'expire_in' => intval(env('JWT_TTL') ?? 1) * 60,
            'token_type' => 'Bearer'
        ];
    }
}
