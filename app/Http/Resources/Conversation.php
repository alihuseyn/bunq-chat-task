<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

/**
 * @OA\Schema(
 *     schema="ConversationResponse",
 *     type="object",
 *     title="Conversation",
 *     properties={
 *         @OA\Property(property="conversation_id", type="integer"),
 *         @OA\Property(property="latest", type="array", @OA\Items(ref="#/components/schemas/MessageResponse"))
 *     }
 * )
 */
class Conversation extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request Request instance
     *
     * @return array
     */
    public function toArray($request)
    {
        $uid = Auth::user()->id;

        return [
            'conversation_id' => $this->conversation_id,
            'latest' => new Message($this)
        ];
    }
}
