<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @OA\Schema(
 *     schema="UserResponse",
 *     type="object",
 *     title="User",
 *     properties={
 *         @OA\Property(property="user_id", type="integer"),
 *         @OA\Property(property="username", type="string")
 *     }
 * )
 */
class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request Request instance
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'user_id' => $this->id,
            'username' => $this->username
        ];
    }
}
