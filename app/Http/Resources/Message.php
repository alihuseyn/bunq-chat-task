<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;

/**
 * @OA\Schema(
 *     schema="MessageResponse",
 *     type="object",
 *     title="Message",
 *     properties={
 *         @OA\Property(property="message_id", type="integer"),
 *         @OA\Property(property="message", type="string"),
 *         @OA\Property(property="timestamp", type="long"),
 *         @OA\Property(property="sender", ref="#/components/schemas/UserResponse"),
 *         @OA\Property(property="receiver", ref="#/components/schemas/UserResponse")
 *     }
 * )
 */
class Message extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request Request instance
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'message_id' => $this->id,
            'message' => $this->message,
            'timestamp' => (new Carbon($this->created_at))->timestamp,
            'sender' => new User($this->sender),
            'receiver' => new User($this->receiver)
        ];
    }
}
