<?php

namespace App\Http\Controllers;

use App\Http\Resources\User as UserResource;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    /**
     * Return current authenticated user information
     *
     * @OA\Get(
     *     path="/auth/me",
     *     summary="Fetch current authenticated user information",
     *     description="Fetch current authenticated user information",
     *     tags={"Auth"},
     *     security={
     *         {"bearerAuth": {}}
     *     },
     *     @OA\Response(
     *         response=200,
     *         description="User Credentials",
     *         @OA\Header(header="X-NEW-TOKEN", ref="#/components/headers/X-NEW-TOKEN"),
     *         @OA\JsonContent(
     *             @OA\Property(property="data", ref="#/components/schemas/UserResponse")
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(ref="#/components/schemas/SimpleErrorResponse")
     *     )
     * )
     *
     * @return Response
     */
    public function me()
    {
        return new UserResource(Auth::user());
    }
}
