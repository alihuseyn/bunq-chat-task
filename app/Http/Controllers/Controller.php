<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    /**
     * @OA\Info(
     *   title="Bunq Chat App",
     *   description="Application allow to make chat with friends over REST",
     *   version="1.0",
     *   @OA\Contact(
     *     email="alihuseyn13@list.ru",
     *     name="Alihuseyn Gulmammadov"
     *   )
     * )
     *
     * @OA\SecurityScheme(
     *     securityScheme="bearerAuth",
     *     in="header",
     *     name="Authorization",
     *     type="http",
     *     scheme="bearer",
     *     bearerFormat="JWT"
     * )
     *
     * @OA\Schema(
     *     schema="SimpleErrorResponse",
     *     type="object",
     *     title="SimpleError",
     *     @OA\Property(
     *         property="errors",
     *         type="object",
     *         {
     *             @OA\Property(property="message", type="string")
     *         }
     *     )
     * )
     *
     * @OA\Header(
     *    header="X-NEW-TOKEN",
     *    description="If token expire in 10 minutes return new one to use for next requests if the response status is success",
     *    @OA\Schema( type="string" )
     * )
     *
     * @OA\Schema(
     *     schema="PaginationLinkResponse",
     *     type="object",
     *     title="PaginationLink",
     *     {
     *             @OA\Property(property="first", type="string"),
     *             @OA\Property(property="last", type="string"),
     *             @OA\Property(property="prev", type="string"),
     *             @OA\Property(property="next", type="string"),
     *     }
     * )
     *
     * @OA\Schema(
     *     schema="PaginationMetaResponse",
     *     type="object",
     *     title="PaginationMeta",
     *     {
     *             @OA\Property(property="current_page", type="integer"),
     *             @OA\Property(property="from", type="integer"),
     *             @OA\Property(property="last_page", type="integer"),
     *             @OA\Property(property="path", type="string"),
     *             @OA\Property(property="per_page", type="integer"),
     *             @OA\Property(property="to", type="integer"),
     *             @OA\Property(property="total", type="integer"),
     *     }
     * )
     */
}
