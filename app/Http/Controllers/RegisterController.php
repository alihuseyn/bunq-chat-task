<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class RegisterController extends Controller
{
    /**
     * Register user to the message application
     *
     * @OA\Post(
     *     path="/auth/register",
     *     summary="Register user to the message application",
     *     description="Register user to the message application",
     *     tags={"Auth"},
     *     @OA\RequestBody(
     *         required=true,
     *         description="User credentials to register",
     *         @OA\JsonContent(
     *             @OA\Property(property="username", type="string"),
     *             @OA\Property(property="password", type="string")
     *         )
     *     ),
     *     @OA\Response(response=201, description="Created"),
     *     @OA\Response(response=400, description="Invalid input")
     * )
     *
     * @param Request $request Instance of Request
     *
     * @return Response
     */
    public function register(Request $request)
    {
        $this->validation($request);
        User::create($request->only(['username', 'password']));

        return response(null, 201);
    }

    /**
     * Validate input
     *
     * @param Request $request Request
     *
     * @throws Exception Validation exception
     *
     * @return void
     */
    protected function validation(Request $request)
    {
        $this->validate($request, [
            'username' => 'required|unique:users,username',
            'password' => 'required|min:6'
        ]);
    }
}
