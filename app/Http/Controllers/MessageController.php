<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Message;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\Conversation as ConversationResource;
use App\Http\Resources\Message as MessageResource;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class MessageController extends Controller
{
    /**
     * Create new message
     *
     * @OA\Post(
     *     path="/conversations/{cid}/messages",
     *     summary="Create new message for already existing conversation",
     *     description="Create new message for already existing conversation",
     *     tags={"Messages"},
     *     security={
     *         {"bearerAuth": {}}
     *     },
     *     @OA\Parameter(
     *         name="cid",
     *         in="path",
     *         description="Conversation Id",
     *         required=true
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         description="Message content",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string")
     *         )
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Created",
     *         @OA\Header(header="X-NEW-TOKEN", ref="#/components/headers/X-NEW-TOKEN")
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Not Found Conversation",
     *         @OA\JsonContent(ref="#/components/schemas/SimpleErrorResponse")
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(ref="#/components/schemas/SimpleErrorResponse")
     *     )
     * )
     *
     * @param Request $request Request Instance
     * @param integer $cid     Conversation id
     *
     * @throws \Exception validation exception
     * @throws NotFoundHttpException
     *
     * @return Response
     */
    public function create(Request $request, $cid)
    {
        $isExists = Message::where('conversation_id', $cid)
            ->conversation()
            ->exists();

        if (!$isExists) {
            throw new NotFoundHttpException;
        }

        $this->validation($request);
        $sender_id = Auth::user()->id;
        $receiver_id = intval($cid) - $sender_id;

        $data = [
            'sender_id' => $sender_id,
            'receiver_id' => $receiver_id,
            'conversation_id' => $cid,
            'message' => $request->message
        ];
        Message::create($data);

        return response(null, 201);
    }

    /**
     * Retrieve detail of message
     *
     * @OA\Get(
     *     path="/conversations/{cid}/messages/{mid}",
     *     summary="Retrieve single message from conversation",
     *     description="Retrieve single message from conversation",
     *     tags={"Messages"},
     *     security={
     *         {"bearerAuth": {}}
     *     },
     *     @OA\Parameter(
     *         name="cid",
     *         in="path",
     *         description="Conversation Id",
     *         required=true
     *     ),
     *     @OA\Parameter(
     *         name="mid",
     *         in="path",
     *         description="Message Id",
     *         required=true
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Detail of message",
     *         @OA\Header(header="X-NEW-TOKEN", ref="#/components/headers/X-NEW-TOKEN"),
     *         @OA\JsonContent(
     *             @OA\Property(
     *                  property="data",
     *                  type="object",
     *                  @OA\Items(ref="#/components/schemas/MessageResponse")
     *              )
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Not Found",
     *         @OA\JsonContent(ref="#/components/schemas/SimpleErrorResponse")
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(ref="#/components/schemas/SimpleErrorResponse")
     *     )
     * )
     * 
     * @param integer $cid conversation identifier
     * @param integer $mid message identifier
     *
     * @throws NotFoundHttpException
     *
     * @return Response
     */
    public function show($cid, $mid)
    {
        $message = Message::where('conversation_id', $cid)
            ->where('id', $mid)
            ->conversation()
            ->first();

        if (empty($message)) {
            throw new NotFoundHttpException;
        }

        return new MessageResource($message);
    }

    /**
     * Soft Delete message with given coversation id
     * and message id it will still be seen for other user
     *
     * @OA\Delete(
     *     path="/conversations/{cid}/messages/{mid}",
     *     summary="Delete selected message from conversation only for authenticated user",
     *     description="Delete selected message from conversation only for authenticated user message will still be visible for other user",
     *     tags={"Messages"},
     *     security={
     *         {"bearerAuth": {}}
     *     },
     *     @OA\Parameter(
     *         name="cid",
     *         in="path",
     *         description="Conversation Id",
     *         required=true
     *     ),
     *     @OA\Parameter(
     *         name="mid",
     *         in="path",
     *         description="Message Id",
     *         required=true
     *     ),
     *     @OA\Response(
     *         response=204,
     *         description="No Content",
     *         @OA\Header(header="X-NEW-TOKEN", ref="#/components/headers/X-NEW-TOKEN")
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Not Found",
     *         @OA\JsonContent(ref="#/components/schemas/SimpleErrorResponse")
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(ref="#/components/schemas/SimpleErrorResponse")
     *     )
     * )
     * 
     * @param string $cid conversation identifier
     * @param string $mid message identifier
     *
     * @throws NotFoundHttpException
     *
     * @return Response
     */
    public function destroy($cid, $mid)
    {
        $isExists = Message::where('conversation_id', $cid)
            ->where('id', $mid)
            ->conversation()
            ->exists();

        if (!$isExists) {
            throw new NotFoundHttpException;
        }

        Message::where('id', $mid)->softDelete();
        return response(null, 204);
    }

    /**
     * Validate input
     *
     * @param Request $request Request
     *
     * @throws Exception Validation exception
     *
     * @return void
     */
    protected function validation(Request $request)
    {
        $this->validate($request, ['message' => 'required|string']);
    }
}
