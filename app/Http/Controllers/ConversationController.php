<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Message;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\Conversation as ConversationResource;
use App\Http\Resources\Message as MessageResource;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Support\Facades\Cache;

class ConversationController extends Controller
{
    /**
     * Return current user all conversations with pagination
     * only top messaging with a user not whole content
     *
     * @OA\Get(
     *     path="/conversations",
     *     summary="Retrieve all latest conversation list",
     *     description="Retrieve all latest conversation list",
     *     tags={"Conversations"},
     *     security={
     *         {"bearerAuth": {}}
     *     },
     *     @OA\Response(
     *         response=200,
     *         description="List of all conversations",
     *         @OA\Header(header="X-NEW-TOKEN", ref="#/components/headers/X-NEW-TOKEN"),
     *         @OA\JsonContent(
     *             @OA\Property(
     *                  property="data",
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/ConversationResponse")
     *              ),
     *              @OA\Property(property="links", ref="#/components/schemas/PaginationLinkResponse"),
     *              @OA\Property(property="meta", ref="#/components/schemas/PaginationMetaResponse"),
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(ref="#/components/schemas/SimpleErrorResponse")
     *     )
     * )
     *
     * @return Response
     */
    public function index()
    {
        $uid = Auth::user()->id;

        $conversations = Cache::rememberForever(
            "conversations_for_$uid",
            function () {
                return $conversations = Message::conversation()
                    ->orderBy('created_at', 'desc')
                    ->groupBy('conversation_id')
                    ->havingRaw('MAX(created_at)')
                    ->get();
            }
        );

        return ConversationResource::collection($conversations->paginate(10));
    }

    /**
     * Create new conversation with generating
     * new message or continue messaging over
     * this method
     *
     * @OA\Post(
     *     path="/conversations",
     *     summary="Create new message with new conversation or already exists",
     *     description="Create new message with new conversation or already exists",
     *     tags={"Conversations"},
     *     security={
     *         {"bearerAuth": {}}
     *     },
     *     @OA\RequestBody(
     *         required=true,
     *         description="Message content and receiver identifier",
     *         @OA\JsonContent(
     *             @OA\Property(property="receiver", type="integer"),
     *             @OA\Property(property="message", type="string")
     *         )
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Created",
     *         @OA\Header(header="X-NEW-TOKEN", ref="#/components/headers/X-NEW-TOKEN")
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Invalid input"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(ref="#/components/schemas/SimpleErrorResponse")
     *     )
     * )
     *
     * @param Request $request Request Instance
     *
     * @throws \Exception validation exception
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $uid = Auth::user()->id;
        $this->validation($request);
        $data = [
            'sender_id' => $uid,
            'receiver_id' => $request->receiver,
            'conversation_id' => ($uid + $request->receiver),
            'message' => $request->message
        ];

        Message::create($data);
        return response(null, 201);
    }

    /**
     * Retrieve detail of conversation
     *
     * Note: Current cache system not support
     * tagging which now allow me to make cache
     * content for specific conversation
     *
     * @OA\Get(
     *     path="/conversations/{cid}",
     *     summary="Retrieve all latest messages under given conversation",
     *     description="Retrieve all latest messages under given conversation",
     *     tags={"Conversations"},
     *     security={
     *         {"bearerAuth": {}}
     *     },
     *     @OA\Parameter(
     *         name="cid",
     *         in="path",
     *         description="Conversation Id",
     *         required=true
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="List of all messages under given conversation",
     *         @OA\Header(header="X-NEW-TOKEN", ref="#/components/headers/X-NEW-TOKEN"),
     *         @OA\JsonContent(
     *             @OA\Property(
     *                  property="data",
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/MessageResponse")
     *              ),
     *              @OA\Property(property="links", ref="#/components/schemas/PaginationLinkResponse"),
     *              @OA\Property(property="meta", ref="#/components/schemas/PaginationMetaResponse"),
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(ref="#/components/schemas/SimpleErrorResponse")
     *     )
     * )
     *
     * @param integer $cid conversation identifier
     *
     * @throws NotFoundHttpException
     *
     * @return Response
     */
    public function show($cid)
    {
        $messages = Message::where('conversation_id', $cid)
            ->where(function ($query) {
                $query->conversation();
            })
            ->orderBy('created_at', 'desc')
            ->paginate(10);

        if (!$messages->total()) {
            throw new NotFoundHttpException;
        }

        return MessageResource::collection($messages);
    }

    /**
     * Soft Delete conversation with given coversation id
     *
     * @OA\Delete(
     *     path="/conversations/{cid}",
     *     summary="Delete selected conversation for current authenticated user",
     *     description="Delete selected conversation for current authenticated user",
     *     tags={"Conversations"},
     *     security={
     *         {"bearerAuth": {}}
     *     },
     *     @OA\Parameter(
     *         name="cid",
     *         in="path",
     *         description="Conversation Id",
     *         required=true
     *     ),
     *     @OA\Response(
     *         response=204,
     *         description="No Content",
     *         @OA\Header(header="X-NEW-TOKEN", ref="#/components/headers/X-NEW-TOKEN")
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Not Found",
     *         @OA\JsonContent(ref="#/components/schemas/SimpleErrorResponse")
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(ref="#/components/schemas/SimpleErrorResponse")
     *     )
     * )
     * 
     * @param string $cid conversation identifier
     *
     * @throws NotFoundHttpException
     *
     * @return Response
     */
    public function destroy($cid)
    {
        $isExists = Message::where('conversation_id', $cid)
            ->conversation()
            ->exists();

        if (!$isExists) {
            throw new NotFoundHttpException;
        }

        Message::where('conversation_id', $cid)->softDelete();
        return response(null, 204);
    }

    /**
     * Validate input
     *
     * @param Request $request Request
     *
     * @throws Exception Validation exception
     *
     * @return void
     */
    protected function validation(Request $request)
    {
        $this->validate($request, [
            'receiver' => [
                'required',
                'exists:users,id',
                function ($attribute, $value, $fail) {
                    if ($value === Auth::user()->id) {
                        $fail('Can\'t write yourself');
                    }
                },
            ],
            'message' => 'required|string'
        ]);
    }
}
