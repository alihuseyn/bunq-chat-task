<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Http\Resources\User as UserResource;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;

class ContactController extends Controller
{
    /**
     * Return list of all available users
     * except current user
     *
     * @OA\Get(
     *     path="/contacts",
     *     summary="List all available contacts",
     *     description="List all available contacts",
     *     tags={"Contacts"},
     *     security={
     *         {"bearerAuth": {}}
     *     },
     *     @OA\Response(
     *         response=200,
     *         description="List of all users",
     *         @OA\Header(header="X-NEW-TOKEN", ref="#/components/headers/X-NEW-TOKEN"),
     *         @OA\JsonContent(
     *             @OA\Property(
     *                  property="data",
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/UserResponse")
     *              )
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(ref="#/components/schemas/SimpleErrorResponse")
     *     )
     * )
     *
     *
     * @return Response
     */
    public function index()
    {
        // Cache will flush on new user addition
        $users = Cache::rememberForever('users', function () {
            return User::all();
        });

        // Exclude current user
        $user = Auth::user();
        $users = $users->where('id', '!=', $user->id);

        return UserResource::collection($users);
    }
}
