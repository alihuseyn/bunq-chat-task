<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\Auth as AuthResource;

class LoginController extends Controller
{
    
    /**
     * Login user to the system
     *
     * @OA\Post(
     *     path="/auth/login",
     *     summary="Login user to the system",
     *     description="Login user to the system",
     *     tags={"Auth"},
     *     @OA\RequestBody(
     *         required=true,
     *         description="User credentials to login",
     *         @OA\JsonContent(
     *             @OA\Property(property="username", type="string"),
     *             @OA\Property(property="password", type="string")
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="User Credentials",
     *         @OA\JsonContent(ref="#/components/schemas/AuthResponse")
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Invalid input"
     *     )
     * )
     *
     * @param Request $request Request instance
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function login(Request $request)
    {
        $this->validation($request);

        if (Auth::attempt($request->only(['username', 'password']))) {
            return new AuthResource(Auth::token());
        }

        throw new \Exception('Username or password is not valid');
    }

    /**
     * Logout user from system
     *
     * @OA\Post(
     *     path="/auth/logout",
     *     summary="Logout user from system not allow to operate with token",
     *     description="Logout user from system not allow to operate with token",
     *     tags={"Auth"},
     *     security={
     *         {"bearerAuth": {}}
     *     },
     *     @OA\Response(response=204, description="No Content"),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(ref="#/components/schemas/SimpleErrorResponse")
     *     )
     * )
     *
     * @param Request $request Request instance
     *
     * @return Response
     */
    public function logout(Request $request)
    {
        Auth::logout();

        return response(null, 204);
    }

    /**
     * Validate input
     *
     * @param Request $request Request
     *
     * @throws Exception Validation exception
     *
     * @return void
     */
    protected function validation(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required'
        ]);
    }
}
