<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Closure;

class JwtWillExpireMiddleware
{
    /**
     * Check after each request to see whether
     * jwt token will expire if then generate
     * new token and return it for further usage
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        $isOk = $response->getStatusCode() >= 200
            && $response->getStatusCode() <= 204;
        if ($isOk && Auth::willExpire()) {
            $response = $response->header('X-NEW-TOKEN', Auth::token());
        }

        return $response;
    }
}
