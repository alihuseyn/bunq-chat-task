<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\User;
use App\Auth\Provider\UserProvider;
use App\Auth\Guard\JWTGuard;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        // Define custom user provider for user class
        $this->app['auth']->provider('bunq', function ($app) {
            return new UserProvider($app['hash'], User::class);
        });

        // Define custom guard named bunq message for custom authentication
        $this->app['auth']->extend('bunq', function ($app, $name, array $config) {
            return new JWTGuard(
                $name,
                $app['auth']->createUserProvider($config['provider']),
                $app->make('request')
            );
        });
    }
}
