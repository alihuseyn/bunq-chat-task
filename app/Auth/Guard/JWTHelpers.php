<?php

namespace App\Auth\Guard;

use Illuminate\Contracts\Auth\Authenticatable;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

class JWTHelpers
{
    /**
     * Base64 url encode
     *
     * @param mixed $text Mixed text content
     *
     * @return string
     */
    protected static function base64UrlEncode($text)
    {
        if (is_array($text)) {
            $text = json_encode($text);
        }

        return str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($text));
    }

    /**
     * Generate a token with claims contain required
     * user identifiers
     *
     * @param Authenticatable $user User Model
     *
     * @return string
     */
    public static function generate(Authenticatable $user)
    {
        $now = Carbon::now();
        $ttl = intval(env('JWT_TTL', 1));

        $header = [
            'typ' => 'JWT',
            'alg' => 'HS256'
        ];
        
        $payload = [
            'iss' => env('JWT_ISS', 'http://localhost'),
            'aud' => env('JWT_ISS', 'http://localhost'),
            'iat' => $now->timestamp,
            'exp' => $now->copy()->addHours($ttl)->timestamp,
            'jti' => env('JWT_JTI', 'abcd-1234-efgh-5678'),
            'uid' => $user->getAuthIdentifier()
        ];

        $signature = hash_hmac(
            'sha256',
            implode(
                '.',
                [
                    self::base64UrlEncode($header),
                    self::base64UrlEncode($payload)
                ]
            ),
            env('JWT_SECRET'),
            true
        );

        return implode(
            '.',
            [
                self::base64UrlEncode($header),
                self::base64UrlEncode($payload),
                self::base64UrlEncode($signature)
            ]
        );
    }

    /**
     * Validate token and throws exception if found any error case
     *
     * @param string $token Token
     *
     * @throws \Exception if any invalid case exists for token
     *
     * @return void
     */
    public static function validate($token)
    {
        // Check whether the access token and refresh token valid
        $parts = explode('.', $token);

        if (count($parts) !== 3) {
            throw new \Exception('Invalid token is given');
        }

        $header = json_decode(base64_decode($parts[0]), true);
        $payload = json_decode(base64_decode($parts[1]), true);
        $signature = $parts[2];

        if (!isset($payload['exp']) || !is_int($payload['exp'])) {
            throw new \Exception('Invalid token is given');
        }

        $expiration = Carbon::createFromTimestamp($payload['exp']);
        if (Carbon::now()->diffInSeconds($expiration, false) < 0) {
            throw new \Exception('Token has expired');
        }

        if ($payload['iss'] != env('JWT_ISS', 'http://localhost')
            && $payload['aud'] != env('JWT_ISS', 'http://localhost')
        ) {
            throw new Exception('Token is not issued from API');
        }

        if ($payload['jti'] != env('JWT_JTI', 'abcd-1234-efgh-5678')) {
            throw new Exception('Token is not identified');
        }

        $_signature = self::base64UrlEncode(
            hash_hmac(
                'sha256',
                implode(
                    '.',
                    [
                        self::base64UrlEncode($header),
                        self::base64UrlEncode($payload)
                    ]
                ),
                env('JWT_SECRET'),
                true
            )
        );

        if ($signature != $_signature) {
            throw new \Exception('Signature is not valid');
        }

        // Check whether given token is blacklisted or not
        if (Cache::has("bl_$token")) {
            throw new \Exception('Token has expired');
        }
    }

    /**
     * Retrieve payload content from given token
     *
     * @param string $token Token
     *
     * @throws \Exception if Token invalid
     *
     * @return array
     */
    public static function payload($token)
    {
        self::validate($token);
        return json_decode(base64_decode(explode('.', $token)[1]), true);
    }

    /**
     * Expire token invalidate it with adding blacklist
     *
     * @param string $token Token
     *
     * @throws \Exception if Token invalid
     *
     * @return void
     */
    public static function expire($token)
    {
        $timestamp = Carbon::now()->timestamp;
        $payload = self::payload($token);
        $remaining = intval(ceil((intval($payload['exp']) - $timestamp) / 60));
        Cache::put("bl_$token", 1, $remaining);
    }

    /**
     * Check whether jwt token will expire in specific
     * time default is 10 minutes
     *
     * @param string $token Token
     *
     * @throws \Exception if Token invalid
     *
     * @return boolean
     */
    public static function willExpire($token)
    {
        $timestamp = Carbon::now()->timestamp;
        $payload = self::payload($token);
        $remaining = intval(ceil((intval($payload['exp']) - $timestamp) / 60));

        return $remaining <= intval(env('JWT_EXP_CHECK', 10));
    }
}
