<?php

namespace App\Auth\Guard;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Traits\Macroable;
use Symfony\Component\HttpFoundation\Request;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Auth\GuardHelpers;
use App\Auth\Guard\JWTHelpers as JWT;

class JWTGuard implements Guard
{
    use Macroable, GuardHelpers;

    /**
     * The name of the Guard. Typically "bunq".
     *
     * Corresponds to guard name in authentication configuration.
     *
     * @var string
     */
    protected $name;

    /**
     * Bunq Message User Provider
     *
     * @var UserProvider
     */
    protected $provider;

    /**
     * Request instance
     *
     * @var \Symfony\Component\HttpFoundation\Request|null
     */
    protected $request;

    /**
     * Indicates if the logout method has been called.
     *
     * @var bool
     */
    protected $loggedOut = false;

    /**
     * Keep jwt token information
     *
     * @var string
     */
    protected $token;

    /**
     * Constructor
     *
     * @param string       $name     Guard name
     * @param UserProvider $provider Bunq User Provider
     * @param Request      $request  Request Object
     *
     * @return void
     */
    public function __construct($name, UserProvider $provider, $request = null)
    {
        $this->name = $name;
        $this->provider = $provider;
        $this->request = $request;
    }

    /**
     * Attempt to authenticate a user using the given credentials.
     *
     * @param array $credentials Credentials content email & password
     *
     * @return bool
     */
    public function attempt(array $credentials = [])
    {
        $user = $this->provider->retrieveByCredentials($credentials);
        $hasValidCredentials = !is_null($user)
            && $this->provider->validateCredentials($user, $credentials);

        if ($hasValidCredentials) {
            $this->setUser($user);
        }

        return $hasValidCredentials;
    }

    /**
     * Generate new token for user and if already
     * created return available
     *
     * @throws \Exception if token can't be generated
     *
     * @return string
     */
    public function token()
    {
        if (!empty($this->token)) {
            return $this->token;
        }

        if (!empty($this->user())) {
            $this->token = JWT::generate($this->user());
            return $this->token;
        }

        throw new \Exception('Token was not generated');
    }

    /**
     * Determine whether a token will get expire in specific defined
     * minutes it is 10 minutes
     *
     * @return boolean
     */
    public function willExpire()
    {
        return JWT::willExpire($this->token ?? $this->getRequest()->bearerToken());
    }

    /**
     * Set the current user.
     *
     * @param \Illuminate\Contracts\Auth\Authenticatable $user User data
     *
     * @return void
     */
    public function setUser(Authenticatable $user)
    {
        $this->user = $user;
        $this->loggedOut = false;

        return $this;
    }

    /**
     * Get the current request instance.
     *
     * @return Request
     */
    public function getRequest()
    {
        return $this->request ?: Request::createFromGlobals();
    }

    /**
     * Set the current request instance.
     *
     * @param Request $request Request
     *
     * @return $this
     */
    public function setRequest(Request $request)
    {
        $this->request = $request;

        return $this;
    }

    /**
     * Get the currently authenticated user.
     *
     * @throws \Exception if the token is invalid
     *
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function user()
    {
        try {
            if ($this->loggedOut) {
                return;
            }

            if (!is_null($this->user)) {
                return $this->user;
            }

            $payload = JWT::payload($this->getRequest()->bearerToken());
            $id = $payload['uid'] ? intval($payload['uid']) : null;

            if (!is_null($id)) {
                $this->user = $this->provider->retrieveById($id);
            }

            return $this->user;
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * Validate a user's credentials.
     *
     * @param array $credentials Credentials
     *
     * @return bool
     */
    public function validate(array $credentials = [])
    {
        $user = $this->provider->retrieveByCredentials($credentials);

        return !is_null($user)
            && $this->provider->validateCredentials($user, $credentials);
    }

    /**
     * Log the user out of the application.
     *
     * @return void
     */
    public function logout()
    {
        $user = $this->user();
        JWT::expire($this->getRequest()->bearerToken());

        $this->user = null;
        $this->loggedOut = true;
    }
}
