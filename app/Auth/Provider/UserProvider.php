<?php

namespace App\Auth\Provider;

use Illuminate\Auth\EloquentUserProvider;
use Illuminate\Contracts\Hashing\Hasher as HasherContract;
use Illuminate\Contracts\Cache\Repository;
use Illuminate\Support\Facades\Cache;

class UserProvider extends EloquentUserProvider
{
    /**
     * Create a new database user provider with cache support.
     *
     * @param Hasher $hasher Hasher implementation
     * @param string $model  User Model
     *
     * @return void
     */
    public function __construct(HasherContract $hasher, $model)
    {
        parent::__construct($hasher, $model);
    }

    /**
     * Retrieve a user by their unique identifier and after first
     * retrieve cache user data in cache repository and retrieve
     * from there for next operations. And remember 60 minutes
     * after first time fetch.
     *
     * @param mixed $identifier Identifier for user
     *
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveById($identifier)
    {
        return Cache::remember(
            "uid_$identifier",
            60,
            function () use ($identifier) {
                $model = $this->createModel();
                return $this->newModelQuery($model)
                    ->where($model->getAuthIdentifierName(), $identifier)
                    ->first();
            }
        );
    }
}
