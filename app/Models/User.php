<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Cache;

class User extends Model implements AuthenticatableContract
{
    use Authenticatable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['username', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * Booted function will be called implicitly
     *
     * @return void
     */
    protected static function booted()
    {
        // Event will triggered when new user created
        static::created(function ($user) {
            // Remove all users cache which
            // will help to retrieve new user
            // to on request to users
            Cache::forget('users');
        });
    }

    /**
     * Set Password Mutator is triggered if definition
     * for password is done and convert it to hashed
     * if required
     *
     * @param string $value Password
     *
     * @return void
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }
}
