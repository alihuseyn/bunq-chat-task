<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;

class Message extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'conversation_id',
        'sender_id',
        'receiver_id',
        'message',
        'deleted_for_sender',
        'deleted_for_receiver',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_for_sender',
        'deleted_for_receiver'
    ];

    /**
     * Booted function will be called implicitly
     *
     * @return void
     */
    protected static function booted()
    {
        if (Auth::check()) {
            $uid = Auth::user()->id;
            // Event will triggered when new message
            // get created or updated
            static::saved(function ($message) use ($uid) {
                // Remove all conversation for current
                Cache::forget("conversations_for_$uid");
            });
        }
    }

    /**
     * Message sender relation
     *
     * @return User
     */
    public function sender()
    {
        return $this->hasOne('App\Models\User', 'id', 'sender_id');
    }

    /**
     * Message receiver relation
     *
     * @return User
     */
    public function receiver()
    {
        return $this->hasOne('App\Models\User', 'id', 'receiver_id');
    }

    /**
     * Custom scope for conversation retrieve
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeConversation($query)
    {
        $uid = Auth::user()->id;

        return $query->where(function ($query) use ($uid) {
            $query->where('sender_id', $uid)
                ->whereNull('deleted_for_sender');
        })
        ->orWhere(function ($query) use ($uid) {
            $query->where('receiver_id', $uid)
                ->whereNull('deleted_for_receiver');
        });
    }

    /**
     * Custom scope for conversation soft delete for sender or receiver
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return integer
     */
    public function scopeSoftDelete($query)
    {
        $uid = Auth::user()->id;
        $now = Carbon::now();
        $query_receiver = clone $query;
        $query_sender = clone $query;

        $query_sender->where(function ($query) use ($uid, $now) {
            $query->where('sender_id', $uid)
                ->whereNull('deleted_for_sender');
        })->update(['deleted_for_sender' => $now]);

        $query_receiver->where(function ($query) use ($uid, $now) {
            $query->where('receiver_id', $uid)
                ->whereNull('deleted_for_receiver');
        })->update(['deleted_for_receiver' => $now]);

        // Trigger cache flush
        event('eloquent.updated: App\Model\Messages');
    }
}
