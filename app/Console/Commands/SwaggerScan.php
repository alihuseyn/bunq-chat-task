<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SwaggerScan extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'swagger:scan';

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'Swagger Scanner';

    /**
     * The console command description.
     *
     * @var string|null
     */
    protected $description = 'Scan directory and generate swagger documentation';


    /**
     * Handle command
     *
     * @return void
     */
    public function handle()
    {
        $path = base_path('app');
        $output = base_path('public/swagger.json');
        
        $this->info('Scanning ' . $path);
        $openApi = \OpenApi\scan($path);

        header('Content-Type: application/json');
        file_put_contents($output, $openApi->toJson());

        $this->info('Output ' . $output);
    }
}
